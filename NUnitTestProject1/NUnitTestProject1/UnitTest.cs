using System;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace NUnitTestProject1
{
    public class UnitTest
    {
        private IWebDriver _driver;
        private IWebElement _webElement;
        private WebDriverWait _driverWait;
        private By _wrapper;

        private const int ActionTime = 3;
        public static class Data
        {
            public static readonly string Name = "Akella"; //Akella
            public static readonly string Key = "WA"; // WA
            public static readonly string Category = "Predator"; //Predator
            public static readonly string Network = "Jungle"; //Jungle
            public static readonly string Group = "SameBlood"; //SameBlood
            public static readonly string Segment = "Animal"; // Animal
            public static List<string> input;
            public static List<string> output;
        }
        public static class MainPaths // ���� �������� ���������
        {
            public static readonly string DriverPath = @"C:\Users\roman\source\repos\NUnitTestProject1\NUnitTestProject1";
            public static readonly string Url = "https://test-task.plarium.com/dashboard";
            public static readonly string MenuButton = "#mat-slide-toggle-1 > label > span";
            public static readonly string DashboardButton = "body > app-root > mat-sidenav-container > mat-sidenav > div > div > button:nth-child(1)";
            public static readonly string OffersButton = "body > app-root > mat-sidenav-container > mat-sidenav > div > div > button:nth-child(2)";
            public static readonly string AddButton = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-list > button";
            public static readonly string OffersPanel = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-list > table > tbody";
        }

        public static class PathsInAddOffer // ���� �������� ��������� � ���� ��������� �����������
        {
            public static readonly string NamePath = "#mat-input-0";
            public static readonly string KeyPath = "#mat-input-1";

            public static readonly string Categories = "#mat-input-20";
            public static readonly string CategoryPath = "#mat-input-2";
            public static readonly string CategoryAddButton = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-form > form > mat-card:nth-child(3) > mat-card-content > mat-form-field.mat-form-field.ng-tns-c13-3.mat-primary.mat-form-field-type-mat-native-select.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-has-label.ng-untouched.ng-pristine.ng-valid.mat-form-field-should-float > div > div.mat-form-field-flex > div.mat-form-field-suffix.ng-tns-c13-3.ng-star-inserted > button > span";

            public static readonly string Networks = "#cdk-overlay-0 > div > div";
            public static readonly string NetworkPath = "#mat-select-0";

            public static readonly string Groups = "#cdk-overlay-1 > div > div";
            public static readonly string GroupPath = "#mat-select-1";

            public static readonly string Segments = "#cdk-overlay-2 > div > div";
            public static readonly string SegmentPath = "#mat-select-2";
            public static readonly string AddSegmentButton = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-form > form > mat-card:nth-child(4) > mat-card-content > app-form-segments > mat-card > mat-card-title > div > button.mat-raised-button.mat-button-base.mat-accent";

            public static readonly string AddGroupInSegmentButton = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-form > form > mat-card:nth-child(4) > mat-card-content > app-form-segments > mat-card > mat-card-title > div > button.mat-raised-button.mat-button-base.mat-primary";
                                                        
            public static readonly string Overlay = "body > div.cdk-overlay-container > div.cdk-overlay-backdrop.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing";
            public static readonly string SaveButton = "body > app-root > mat-sidenav-container > mat-sidenav-content > app-form > form > mat-card:nth-child(1) > mat-card-title > div > button";
        }

        [Test]
        public void AddOffer() //������� ����������� � ������� �� �������
        {
            Initialize();
            ClickButton(MainPaths.MenuButton, ActionTime);
            ClickButton(MainPaths.OffersButton, ActionTime);
            ClickButton(MainPaths.AddButton, ActionTime);
            Wait(1);

            EnterKeys(PathsInAddOffer.NamePath, 1, Data.Name);
            EnterKeys(PathsInAddOffer.KeyPath, 1, Data.Key);

            InputEnter(PathsInAddOffer.CategoryPath, 1);
            ReadingText(PathsInAddOffer.CategoryPath, "option", Data.Category, 1);

            InputEnter(PathsInAddOffer.NetworkPath, 1);
            ReadingText(PathsInAddOffer.Networks, "mat-option", Data.Network, 1);

            InputEnter(PathsInAddOffer.GroupPath, 1);
            ReadingText(PathsInAddOffer.Groups, "mat-option", Data.Group, 1);

            InputEnter(PathsInAddOffer.AddSegmentButton, 1);
            InputEnter(PathsInAddOffer.SegmentPath, 1);
            ReadingText(PathsInAddOffer.Segments, "mat-option", Data.Segment, 1);

            InputEnter(PathsInAddOffer.AddGroupInSegmentButton, 1);

            InputEnter(PathsInAddOffer.SaveButton, 1);
            _driver.Quit();
        }
        [Test]
        public void ReadInfo() // ������ ������ �� �����������
        {
            Initialize();
            ClickButton(MainPaths.MenuButton, ActionTime);
            ClickButton(MainPaths.OffersButton, ActionTime);
            ReadingButton(MainPaths.OffersPanel, 0, Data.Name, Data.Key, 1); //����� �� ����� � �����, ������� �� ������ "Edit"

            string nameResult = GetElementText(PathsInAddOffer.NamePath);

            string keyResult = GetElementText(PathsInAddOffer.KeyPath);

            string categoryResult = GetElementText(PathsInAddOffer.CategoryPath);

            string networkResult = GetElementText(PathsInAddOffer.NetworkPath);

            string groupResult = GetElementText(PathsInAddOffer.GroupPath);

            string segmentResult = GetElementText(PathsInAddOffer.SegmentPath);

            Data.output = new List<string>() { nameResult, keyResult, categoryResult.Trim(), networkResult, groupResult, segmentResult };
            _driver.Quit();// Trim - ������� �������, �� ������-�� � ����� ��������� ���������� � ������������� ���������, � � ������ ���
        }
        [Test]
        public void Remove() //������� ���������
        {
            Initialize();
            ClickButton(MainPaths.MenuButton, ActionTime);
            ClickButton(MainPaths.OffersButton, ActionTime);
            ReadingButton(MainPaths.OffersPanel, 1, Data.Name, Data.Key, 1); //����� �� ����� � �����, ������� �� ������ "Edit"
            var element = _driver.FindElement(By.XPath(@"/html/body/div[2]/div/div/snack-bar-container/simple-snack-bar/div/button"));
            element.Click();
            _driver.Quit();
        }
        [Test]
        public void Result() // ���������� ����������
        {
            Data.input = new List<string>() { Data.Name, Data.Key, Data.Category, Data.Network, Data.Group, Data.Segment };
            Assert.AreEqual(Data.input, Data.output);
        }
        
        public string GetElementText(string path, int seconds = 2)
        {
            Wait(seconds);

            string result = "";
            var element = _driver.FindElement(By.CssSelector(path));
            string tag = element.TagName.ToLower();

            switch (tag)
            {
                case "input":
                    result = element.GetAttribute("value");
                    break;
                case "select":
                    result = new SelectElement(element).SelectedOption.Text;
                    break;
                default:
                    result = element.Text;
                    break;
            }
            return result;
        }
        
        public void ReadingButton(string path, int id, string name, string key, int seconds)
        {
            Wait(seconds);
            IWebElement buttons = null;
            var element = _driver.FindElement(By.CssSelector(path));
            IList<IWebElement> elementsList = element.FindElements(By.TagName("td"));
            for (int i = 0; i < elementsList.Count; i++)
                if (name == elementsList[i].Text && i < elementsList.Count - 1)
                    if (key == elementsList[i + 1].Text)
                    {
                        buttons = elementsList[i + 2];
                        break;
                    }
            IList<IWebElement> buttonsList = buttons.FindElements(By.TagName("button"));
            buttonsList[id].Click();
        }
        public void ReadingText(string path, string tag, string dataUnit, int seconds)
        {
            Wait(seconds);
            var element = _driver.FindElement(By.CssSelector(path));
            IList<IWebElement> elementsList = element.FindElements(By.TagName(tag));
            foreach (var unit in elementsList)
                if (dataUnit == unit.Text || " " + dataUnit + " " == unit.Text)
                {
                    unit.Click();
                    break;
                }
        }  

        private void Initialize()
        {
            _driver = new ChromeDriver (MainPaths.DriverPath);
            _driverWait = new WebDriverWait(_driver, TimeSpan.Zero);
            _driver.Navigate().GoToUrl(MainPaths.Url);
        }
        private void EnterKeys(string path, int seconds, string key)
        {
            Wait(seconds);
            _wrapper = By.CssSelector(path);
            _webElement = _driver.FindElement(_wrapper);
            _webElement.SendKeys(key);
        }
        private void InputEnter(string path, int seconds)
        {
            Wait(seconds);
            _wrapper = By.CssSelector(path);
            _webElement = _driver.FindElement(_wrapper);
            _webElement.SendKeys(Keys.Enter);
        }
        private void ClickButton(string path, int seconds)
        {
            Wait(seconds);
            _wrapper = By.CssSelector(path);
            _webElement = _driver.FindElement(_wrapper);
            _webElement.Click();
        }

        private void Wait(int seconds)
        {
            _driverWait.Timeout = TimeSpan.FromSeconds(seconds);
            var timestamp = DateTime.Now;
            _driverWait.Until(_driver => (DateTime.Now - timestamp) > _driverWait.Timeout);
        }
       
    }
}

